from dataclasses import dataclass
from datetime import datetime
from typing import Set


@dataclass
class Plan:
    name: str
    organizer: str
    date: datetime
    attendees: Set[str]

    def join(self, name):
        self.attendees.add(name)

    def leave(self, name):
        try:
            self.attendees.remove(name)
        except ValueError:
            logging.error(
                f"Failed to find {name} from attendees list for plan {self.name}. Aborting leaving."
            )

    def notify(self):
        print(f"{self.name} is now. Attendees {self.attendees}")

