import logging
import queue

from datetime import datetime, timezone, timedelta
from multiprocessing import Process, Queue
from threading import Timer
from time import sleep

from plan import Plan


class LunchBot(Process):

    def __init__(self, queue):
        super(LunchBot, self).__init__()
        self.queue = queue
        self.execute = True
        self.timers = dict()

    def create_timer(self, date):
        delay = date - datetime.now(timezone.utc)
        logging.info(f"Creating Timer with delay: {delay}")
        Timer(delay.total_seconds(), send_notify, args=(self.queue, date)).start()

    def create_plan(self, name, organizer, date, attendees=set()):
        date = date.replace(second=0, microsecond=0)
        plan = Plan(name, organizer, date, attendees)
        try:
            self.timers[date].append(plan)
        except KeyError:
            logging.debug(f"No timer found for {date}. Adding it.")
            self.create_timer(date)
            self.timers[date] = [plan]

    def delete_plan(self, plan):
        try:
            self.timers[plan.date].remove(plan)
        except ValueError:
            logging.error(
                f"Failed to find plan {plan.name} in list of existing plans. Aborting deletion."
            )
        except KeyError:
            logging.error(
                f"Failed to find timer entry {plan.date} for plan {plan.name}. Aborting deletion."
            )

    def update_plan(self, plan, name=None, organizer=None, date=None):
        if name:
            plan.name = name

        if organizer:
            plan.organizer = organizer

        if date:
            date = date.replace(second=0, microsecond=0)
            plan.date = date
            try:
                self.timers[date].append(plan)
            except KeyError:
                logging.debug(f"No timer found for {date}. Adding it.")
                self.create_timer(date)
                self.timers[date] = [plan]

    def list_plans(self):
        return [plan for plans in self.timers.values() for plan in plans]

    def get_plan_by_name(self, name):
        for plans in self.timers.values():
            for plan in plans:
                if name == plan.name:
                    return plan

    def get_plan_by_organizer(self, organizer):
        for plans in self.timers.values():
            for plan in self.plans:
                if organizer == plan.organizer:
                    return plan

    def find_plan(self, name=None, organizer=None):
        if name:
            return self.get_plan_by_name(name)
        elif organizer:
            return self.get_plan_by_organizer(name)

    def notify(self, date):
        try:
            plans = self.timers[date]
            for plan in plans:
                plan.notify()
        except KeyError:
            logging.warn(f"Received NOTIFY event for an unrecorded date. date: {date}")

    def run(self):
        logging.debug("Start listening from queue")
        while self.execute:
            try:
                ev_type, *event = self.queue.get(block=False)
                if ev_type == "STOP":
                    logging.info("STOP event received.")
                    slef.execute = false
                elif ev_type == "NOTIFY":
                    logging.info("NOTIFY event received.")
                    self.notify(*event)
                elif ev_type == "CREATE":
                    logging.info("CREATE event received.")
                    self.create_plan(*event)
                elif ev_type == "DELETE":
                    logging.info("DELETE event received.")
                    self.delete_plan(*event)
                elif ev_type == "UPDATE":
                    logging.info("UPDATE event received.")
                    self.update_plan(*event)
                elif ev_type == "JOIN":
                    logging.info("JOIN event received.")
                    joiner, plan_identifier = event
                    plan = self.find_plan(**plan_identifier)
                    plan.attendees.add(joiner)
                elif ev_type == "LEAVE":
                    logging.info("LEAVE event received.")
                    joiner, plan_identifier = event
                    plan = self.find_plan(**plan_identifier)
                    plan.attendees.remove(joiner)
            except queue.Empty:
                logging.info(f"Queue is empty sleeping for a bit.")
                sleep(10)

def send_notify(queue, date):
    logging.info(f"Sending notification.")
    queue.put(("NOTIFY", date))

