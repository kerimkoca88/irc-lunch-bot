import unittest

import plan as sut

class TestPlan(unittest.TestCase):

    def test_create_plan(self):
        test_plan_name = "test_plan_name"
        test_plan_organizer = "test_plan_organizer"
        test_plan_date = "test_plan_date"
        test_plan_attendees = set()

        result = sut.Plan(test_plan_name, test_plan_organizer, test_plan_date, test_plan_attendees)

        self.assertEqual(result.name, test_plan_name)
        self.assertEqual(result.organizer, test_plan_organizer)
        self.assertEqual(result.date, test_plan_date)

    def test_join_plan(self):
        test_plan_name = "test_plan_name"
        test_plan_organizer = "test_plan_organizer"
        test_plan_date = "test_plan_date"
        test_plan_attendees = set()
        test_attendee = "test_attendee"

        test_plan = sut.Plan(test_plan_name, test_plan_organizer, test_plan_date, test_plan_attendees)

        test_plan.join(test_attendee)

        self.assertIn(test_attendee, test_plan.attendees)

    def test_leave_plan(self):
        test_plan_name = "test_plan_name"
        test_plan_organizer = "test_plan_organizer"
        test_plan_date = "test_plan_date"
        test_plan_attendees = set()
        test_attendee = "test_attendee"

        test_plan = sut.Plan(test_plan_name, test_plan_organizer, test_plan_date, test_plan_attendees)

        test_plan.join(test_attendee)
        test_plan.leave(test_attendee)

        self.assertNotIn(test_attendee, test_plan.attendees)

